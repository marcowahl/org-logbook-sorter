;;; logbook-sorter.el --- Sort the items of an Org logbook drawer -*- lexical-binding : t ; eval: (read-only-mode) -*-

;; [[id:0925add6-f4d3-485d-8979-f34c557e9487][Firstlines:2]]

(require 'org)
;; Firstlines:2 ends here

;; Decorate
;; :PROPERTIES:
;; :ID:       85ece86a-641b-44ba-bdec-de954f66f802
;; :END:


;; [[id:85ece86a-641b-44ba-bdec-de954f66f802][Decorate:1]]

(defun org-plain-item-prefix-clock-lines ()
  (save-excursion
    (while  (search-forward-regexp org-clock-string nil t)
      (save-excursion
        (goto-char (match-beginning 0))
        (insert "- ")))))
;; Decorate:1 ends here

;; Undecorate
;; :PROPERTIES:
;; :ID:       305121ad-4400-4af2-a5d5-4f24eb02aa34
;; :END:


;; [[id:305121ad-4400-4af2-a5d5-4f24eb02aa34][Undecorate:1]]

(defun org-plain-item-unprefix-clock-lines ()
  (save-excursion
    (while  (search-forward-regexp "- CLOCK: " nil t)
      (save-excursion
        (let* ((start (match-beginning 0))
               (end (+ (length "- ") start)))
          (delete-region start end))))))
;; Undecorate:1 ends here

;; Narrow to Drawer
;; :PROPERTIES:
;; :ID:       6c80e937-ab6f-4f2e-a72c-6674ffd3b8bc
;; :END:


;; [[id:6c80e937-ab6f-4f2e-a72c-6674ffd3b8bc][Narrow to Drawer:1]]

(defun org-narrow-to-drawer ()
  (let ((element (org-element-at-point)))
    (catch 'found
      (while  element
        (when (and (eq 'drawer (car element))
                   (string-equal "LOGBOOK" (plist-get (cadr element)
                                                      :drawer-name))
                   (< (point)
                      (- (plist-get (cadr element) :end)
                         (plist-get (cadr element) :post-blank))))
          (narrow-to-region
           (org-element-property :contents-begin element)
           (org-element-property :contents-end element))
          (throw 'found nil) ;; quit.  no interest in further parent elements.
          )
        (setq element (plist-get (cadr element) :parent))))))
;; Narrow to Drawer:1 ends here

;; Inside Drawer
;; :PROPERTIES:
;; :ID:       1bc5f04e-0ceb-4e4c-815d-9232aa3ca662
;; :END:


;; [[id:1bc5f04e-0ceb-4e4c-815d-9232aa3ca662][Inside Drawer:1]]
(defun org-inside-logbook-drawer-p ()
  (let ((element (org-element-at-point)))
    (catch 'found
      (while  element
        (when (and (eq 'drawer (car element))
                   (string-equal "LOGBOOK" (plist-get (cadr element)
                                                      :drawer-name))
                   (< (point)
                      (- (plist-get (cadr element) :end)
                         (plist-get (cadr element) :post-blank))))
          (throw 'found 'found))
        (setq element (plist-get (cadr element) :parent))))))
;; Inside Drawer:1 ends here

;; Sort LOGBOOK Drawer
;; :PROPERTIES:
;; :ID:       d0b25ddf-bcfb-4396-9162-bbaea5211507
;; :END:


;; [[id:d0b25ddf-bcfb-4396-9162-bbaea5211507][Sort LOGBOOK Drawer:1]]
(defun org-sort-logbook ()
  "On a LOGBOOK drawer sort its contents according to the dates."
  (interactive)
  (when (org-inside-logbook-drawer-p)
    (save-restriction
      (org-narrow-to-drawer)
      (goto-char (point-min))
      (org-plain-item-prefix-clock-lines)
      (org-sort-list nil ?T)
      (org-plain-item-unprefix-clock-lines))))
;; Sort LOGBOOK Drawer:1 ends here

;; Merge LOGBOOK entries up
;; :PROPERTIES:
;; :ID:       8f26146c-2481-4bd1-91ae-13e0bd3f797d
;; :END:

;; This has been an answer for
;; https://emacs.stackexchange.com/questions/41393/merging-clock-logs-together-in-org-mode


;; [[id:8f26146c-2481-4bd1-91ae-13e0bd3f797d][Merge LOGBOOK entries up:1]]
(defun mw-org-merge-logbook-drawer-entries-up ()
  "Remove logbook entries of current subtree and merge into parent.
This function is a start.  It's almost untested."
  (interactive)
  (let* ((beg (org-log-beginning))
         (end (save-excursion
                (goto-char beg)
                (save-match-data
                  (re-search-forward ":end:")
                  (beginning-of-line)
                  (point))))
         (log-content (buffer-substring beg end)))
    (delete-region beg end)
    (org-remove-empty-drawer-at beg)
    (outline-up-heading 1)
    (goto-char (org-log-beginning 'create))
    (insert log-content))
  ;; for the next line see
  ;; https://github.com/marcowahl/org-logbook-sorter/blob/master/logbook-sorter.el
  ;; .  leave out the line if you don't care about sorting the logbook entries.
    (org-sort-logbook))

(defalias 'mw-org-logbook-merge-entries-up 'mw-org-merge-logbook-drawer-entries-up)
;; Merge LOGBOOK entries up:1 ends here


(provide 'logbook-sorter)


;;; logbook-sorter.el ends here
